// ==UserScript==
// @version      2
// @name         fuck git memory
// @namespace    azabani.com
// @author       Delan Azabani <delan@azabani.com>
// @updateURL    https://bitbucket.org/delan/fuckgitmemory/raw/default/index.user.js
// @source       https://bitbucket.org/delan/fuckgitmemory
// @match        https://www.google.com/search?*
// @match        https://githubmemory.com/repo/*
// @match        https://gitmemory.com/issue/*
// @match        https://issueexplorer.com/issue/*
// @match        https://uonfu.com/q/*
// @grant        none
// @license      ISC
// ==/UserScript==

const BAD = {
    "githubmemory.com": {pathPrefix: "/repo/", userIndex: 2, repoIndex: 3, issueIndex: 5},
    "gitmemory.com": {pathPrefix: "/issue/", userIndex: 2, repoIndex: 3, issueIndex: 4},
    "issueexplorer.com": {pathPrefix: "/issue/", userIndex: 2, repoIndex: 3, issueIndex: 4},
    "uonfu.com": {pathPrefix: "/q/", userIndex: 2, repoIndex: 3, issueIndex: 4},
};

if (location.pathname == "/search") {
    // Google search page
    document.querySelectorAll("a > h3").forEach(x => {
        x = x.parentNode;
        const result = fix(x);
        if (result != null) {
            x.href = result;
            // suppress the rewriting to redirect, which breaks if the href is different
            x.addEventListener("mousedown", event => void event.stopPropagation(), true);
            // fix the URL breadcrumbs above the main result link, *without* relying on any
            // generated class names (div.g looks safe to rely on)
            while (x && !x.classList.contains("g"))
                x = x.parentNode;
            x.querySelectorAll("cite").forEach(cite => {
                const url = new URL(result);
                const first = url.origin;
                const rest = url.pathname.split("/").join(` › `);
                const span = cite.lastElementChild.cloneNode();
                cite.textContent = first;
                span.textContent = rest;
                cite.append(span);
            });
        }
    });
} else {
    const result = fix(location);
    if (result != null) {
        location.replace(result);
    }
}

function fix(urlish) {
    if (BAD.hasOwnProperty(urlish.hostname)) {
        const {pathPrefix, userIndex, repoIndex, issueIndex} = BAD[urlish.hostname];
        if (urlish.pathname.startsWith(pathPrefix)) {
            const components = urlish.pathname.split("/");
            const [user, repo, issue] = [userIndex, repoIndex, issueIndex].map(x => components[x]);
            return `https://github.com/${user}/${repo}/issues/${issue}`;
        }
    }
    return null;
}
